import React, { Component } from 'react';
import './styles/bootstrap.min.css';
import './styles/style.css';

import inputProps from "./client/components/inputRows/components/inputProps/inputProps";

import { Header } from "./client/components/header/components/Header";
import { InputForm } from "./client/components/inputRows/InputForm";
import { BookList } from "./client/components/BookList";

export default class App extends Component {
  state = {
    books: []
  };

  // ACTIONS
  addToList = (title, author, isbn) => {
    const booksArray = this.state.books;
    const bookId = booksArray.length > 0 ? booksArray[booksArray.length - 1].bookId + 1 : 1;
    const newBook = { bookId, title, author, isbn };
    this.setState(({ books }) => {
      return {
        books: [...books, newBook]
      };
    });
  };

  updateListItems = (bookId, title, author, isbn) => {
    this.setState(({ books }) => {
      const newBooks = [...books];
      const idx = newBooks.findIndex(item => item.bookId === bookId);
      newBooks[idx] = { bookId, title, author, isbn };
      return {
        books: newBooks
      };
    });
  };

  removeFromList = (index) => {
    this.setState(({ books }) => {
      const newBooks = [...books];
      const idx = newBooks.findIndex(item => item.bookId === index);
      newBooks.splice(idx, 1);
      return {
        books: newBooks
      };
    });
  };

  // RENDER
  render() {
    const { inputsForBooks } = inputProps;

    return (
        <div className="App">
          <div className="container">
            <Header />
            <InputForm { ...inputsForBooks } addToList={ this.addToList } />
            <BookList books={ this.state.books }
                      updateListItems={ this.updateListItems }
                      removeFromList={ this.removeFromList } />
          </div>
        </div>
    );
  };
};
