import React from 'react';

export const EditBtn = () => {
    return (
        <a href="#" className="btn btn-info btn-sm"><i className="fas fa-edit" /></a>
    );
};
