import React from 'react';

export const SubmitBtn = () => {
    return (
        <>
            <input type="submit" value="Add Book" className="btn btn-primary" />
        </>
    );
};
