import React from 'react';

export const DeleteBtn = () => {
    return (
        <a href="#" className="btn btn-danger btn-sm btn-delete"> X </a>
    );
};
