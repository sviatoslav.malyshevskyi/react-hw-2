import React from 'react';

export const UpdateBtn = () => {
    return (
        <input type="submit" value="Update" className="btn btn-primary" />
    );
};
