import React from 'react';
import { BookListItem } from "./BookListItem";

export const BookList = (props) => {
  const booksArray = props.books;
  const BookList = booksArray.map(element =>
      <BookListItem key={ element.bookId }
                    { ...element }
                    updateListItems={ props.updateListItems }
                    removeFromList={ props.removeFromList }
      />);

  return (
      <>
        <h3 id="book-count" className="book-count mt-5">Всего книг: { booksArray.length }</h3>

        <table className="table table-striped mt-2">
          <thead>
          <tr>
            <th>Title</th>
            <th>Author</th>
            <th>ISBN#</th>
          </tr>
          </thead>
          <tbody>
          { BookList }
          </tbody>
        </table>
      </>
  );
};
