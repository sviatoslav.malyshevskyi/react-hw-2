import React, { Component } from 'react';

export class BookListItem extends Component {
  state = {
    title: this.props.title,
    author: this.props.author,
    isbn: this.props.isbn,
    bookId: this.props.bookId,
    edit: false,
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleEditListItems = (event) => {
    event.preventDefault();
    this.setState({ edit: true });
  };

  handleUpdateListItems = (event) => {
    event.preventDefault();
    const { bookId, title, author, isbn } = this.state;
    this.props.updateListItems(bookId, title, author, isbn);
    this.setState({ edit: false });
  };

  handleRemoveFromList = (event) => {
    event.preventDefault();
    const { bookId, removeFromList } = this.props;
    removeFromList(bookId);
  };

  render () {
    if (!this.state.edit) {
      const { bookId, title, author, isbn } = this.props;

      return (
          <tr data-id={ bookId }>
            <td>{ title }</td>
            <td>{ author }</td>
            <td>{ isbn }</td>
            <td><a href="#" className="btn btn-info btn-sm" onClick={ this.handleEditListItems }>
              <i className="fas fa-edit" />
            </a></td>
            <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={ this.handleRemoveFromList }>X</a></td>
          </tr>
      )
    } else {
      const { bookId, title, author, isbn } = this.state;
      return (
          <tr data-id={ bookId }>
            <td><input type="text" value={ title } name="title" onChange={ this.handleChange } required /></td>
            <td><input type="text" value={ author } name="author" onChange={ this.handleChange } required /></td>
            <td><input type="text" value={ isbn } name="isbn" onChange={ this.handleChange } required /></td>
            <td><input type="submit" value="Update" className="btn btn-primary" onClick={ this.handleUpdateListItems } /></td>
          </tr>
      );
    }
  };
}