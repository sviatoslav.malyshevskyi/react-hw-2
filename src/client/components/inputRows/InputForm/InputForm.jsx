import React, { Component } from 'react';
import { InputRows } from '../components/InputRows';

export class InputForm extends Component {
  state = {
    title: '',
    author: '',
    isbn: '',
  };

  handleChange = (event) => {
    this.setState({[ event.target.name ]: event.target.value });
  };

  handleSubmitItemToList = (event) => {
    event.preventDefault();
    const { title, author, isbn } = this.state;
    this.props.addToList(title, author, isbn);
    this.setState({ title: '', author: '', isbn: '' })
  };

  render() {
    const { title, author, isbn, submit } = this.props;

    return (
        <div className="row ml-5">
          <div className="col-lg-4 ml-5">
            <form id="add-book-form" onSubmit={ this.handleSubmitItemToList }>
              <InputRows { ...title } value={ this.state.title } handleChange={ this.handleChange } />
              <InputRows { ...author } value={ this.state.author } handleChange={ this.handleChange } />
              <InputRows { ...isbn } value={ this.state.isbn } handleChange={ this.handleChange } />
              <InputRows { ...submit } />
            </form>
          </div>
        </div>
    );
  };
}
