const inputProps = {
  inputsForBooks: {
    title: {
      type: 'text',
      name: 'title',
      label: 'Title',
      htmlFor: 'title',
      className: 'form-control',
    },
    author: {
      type: 'text',
      name: 'author',
      label: 'Author',
      htmlFor: 'author',
      className: 'form-control',
    },
    isbn: {
      typ: 'text',
      name: 'isbn',
      label: "ISBN#",
      htmlFor: 'title',
      className: 'form-control',
    },
    submit: {
      type: 'submit',
      value: 'Add Book',
      className: 'btn btn-primary'
    },
  },
};

export default inputProps;