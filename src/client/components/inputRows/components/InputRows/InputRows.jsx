import React from 'react';

export const InputRows = (props) => {
  const { type, name, value, label, className, htmlFor, handleChange } = props;

  return (
      <div className="form-group">
        <label htmlFor={ htmlFor } > { label }</label>
        <input type={ type } name={ name } value={ value }
               className={ className } onChange={ handleChange } required={ true } />
      </div>
  );
};