import React from 'react';

export const Header = () => { 
    return (
        <>
            <h1 className="display-4 text-center">
                <i className="fas fa-book-open text-primary mr-2" />
                <span className="text-secondary">Book</span> List
            </h1>
            <p className="text-center">
                Add your book information to store it in the database.
            </p>
        </>
    );
};
